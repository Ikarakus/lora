import firebase from "firebase/compat/app";
import  "firebase/compat/firestore";
import "firebase/compat/auth";
const firebaseConfig = {
    apiKey: "AIzaSyD8VAZnyaZt0CsITUiSmwnRwrVyD7DNc4s",
    authDomain: "simplecommerce-8bb29.firebaseapp.com",
    projectId: "simplecommerce-8bb29",
    storageBucket: "simplecommerce-8bb29.appspot.com",
    messagingSenderId: "144279712888",
    appId: "1:144279712888:web:3f46df04b4c7a53dffa98d",
    measurementId: "G-PSE6KC3HX8"
  };
firebase.initializeApp(firebaseConfig);

export default firebase




// export default firebase;

// import { initializeApp } from "firebase/app";
// import { getStorage  } from "firebase/storage";
// import { getFirestore } from "firebase/firestore";
// import { getAuth, signInWithEmailAndPassword  } from "firebase/auth"

// const firebaseConfig = {
//   apiKey: "AIzaSyD8VAZnyaZt0CsITUiSmwnRwrVyD7DNc4s",
//   authDomain: "simplecommerce-8bb29.firebaseapp.com",
//   projectId: "simplecommerce-8bb29",
//   storageBucket: "simplecommerce-8bb29.appspot.com",
//   messagingSenderId: "144279712888",
//   appId: "1:144279712888:web:3f46df04b4c7a53dffa98d",
//   measurementId: "G-PSE6KC3HX8",
// };

// const defaultProject = initializeApp(firebaseConfig);
// let defaultStorage = getStorage(defaultProject);
// let defaultFirestore = getFirestore(defaultProject);

// // Option 2: Access Firebase services using shorthand notation
// defaultStorage = getStorage();
// defaultFirestore = getFirestore();

// const fbase = {
//     defaultFirestore,
//     defaultFirestore,
//     getAuth,
//     signInWithEmailAndPassword
// }

// export default fbase;
