// const paymentHost = "https://lorapay.ozdisan.com/";
// const uiHost = "https://lora.ozdisan.com/"

const paymentHost = "http://localhost:9280/";
// const uiHost = "http://172.16.97.152:8080/";

const uiHost = "http://192.168.1.101:8080/";

export default {
  ui: {
    paths: {
      paymentResult: uiHost + "pages/dsform/result/",
    },
  },
  payment: {
    paths: {
      checkBin: paymentHost + "payment/checkBin",
      threedsInitialize: paymentHost + "payment/threedsInitialize",
      threedsInitialize: paymentHost + "payment/threedsInitialize",
      threedsInitializeResponse:
        paymentHost + "payment/threedsInitializeResponse",
      paymentRetrieve: paymentHost + "payment/paymentRetrieve",
    },
  },
  orderchecker:{
    sendConfirmSms: paymentHost + "orderchecker/sendConfirmSms",
    sendSmsConfirmRequest: paymentHost + "orderchecker/sendSmsConfirmRequest",

  }
};
