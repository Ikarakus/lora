const user = {
  id: null,
  sysUserId: null,
  name: null,
  lastName: null,
  phone: null,
  email: null,
  password: null,
  createdDate: null,
  updateDate: null,
  createdIpAddress: null,
  commerciallawagreement: false,
  membershipcontionagreement: true,
  clientConnection:null
};

export default user;
